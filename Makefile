FILES := $(shell fd '.scad$$' --type=file --exclude "lib/**/*.scad" --exclude "deps/**/*.scad" | sd '^(.+).scad$$' 'out/$$1.stl')

options:
	@echo "---| Options |---"
	@echo "FILES  $(FILES)"
	@echo "HOOKS  $(HOOKS)"

out/%.stl: %.scad
	mkdir -p "$$(dirname "$@")"
	openscad $^ -o $@ -D '$$fn=360'

all: $(FILES)

.PHONY: options
