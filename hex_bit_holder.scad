/* config */

nrows = 14;
ncols = 4;

_base_height = 10;
base_fillet = 1;
base_wall = 3;
base_floor = 2;

_bit_size = 2; // S2

expansion = 0.15;

slots_cut_walls = true;

$fn = 90;

/* config end */

use <./lib/bits.scad>;
use <./lib/shapes.scad>;

base_height = _base_height + base_floor;
bit_size = _bit_size + expansion;

bit_radius = bit_size / cos(30);
bit_depth = 2 * bit_size;
bit_width = 2 * bit_radius;

difference() {
	// base
	base_depth = (base_wall + bit_depth) * nrows + base_wall;
	base_width = base_wall + (base_wall + bit_width) * ncols;
	rounded_cube([base_width, base_depth, base_height], base_fillet);

	// bit holes
	translate([base_wall + bit_radius, base_wall + bit_size, base_floor]) {
		x_step = base_wall + bit_width;
		y_step = base_wall + bit_depth;

		for (i = [0 : ncols - 1]) {
			for (j = [0 : nrows - 1] ) {
				translate([x_step * i, y_step * j, 0])
				hex_bit(ir=bit_size, h=base_height);
			}
		}
	}

	// slots
	translate([0, (base_wall + bit_size) - bit_size / 2, base_floor]) {
		y_step = base_wall + bit_depth;

		for (j = [0 : nrows - 1]) {
			if (slots_cut_walls) {
				translate([-1, y_step * j, 0])
				cube([base_width + 2, bit_depth / 2, base_height]);
			} else {
				translate([base_wall / 2, y_step * j, 0])
				cube([(base_wall + bit_width) * ncols, bit_depth / 2, base_height]);
			}
		}
	}
}

