$fn = 90;

include <./deps/BOSL/constants.scad>
use <./deps/BOSL/transforms.scad>
use <./deps/BOSL/shapes.scad>


// the inner diameter should equal to the lamp's outer diameter which is 23mm
inner_diameter = 23.0;
thickness = 2.0;
cap_offset = 5.0;
cap_depth = 10.0;

monitor_thickness = 19.0;
clamp_thickness = thickness + cap_depth;
clamp_length_back = 40.0;
clamp_length_front = 5.0;

difference()
{
	// cap
	hull()
	{
		color("#16A0857F")
		right(inner_diameter / 2)
		back(inner_diameter / 2)
		cube([thickness, monitor_thickness + 2 * thickness, clamp_thickness]);

		color("#F39C127F")
		cylinder(h=cap_depth + thickness, d=inner_diameter + 2*thickness);
	};

	// lamp slot
	color("#9B59B67F")
	up(thickness)
	cylinder(h=cap_depth + thickness, d=inner_diameter);
};

back(inner_diameter / 2)
right(inner_diameter / 2 + thickness)
{
	cube([clamp_length_front, thickness, clamp_thickness]);

	back(monitor_thickness + thickness)
	cube([clamp_length_back, thickness, clamp_thickness]);
};
