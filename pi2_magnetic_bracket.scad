/* config */

_pi_height = 31;
_pi_length = 93.5;
_pi_radius = 5.75;
_pi_width  = 62;

_pi_io_height = 16;
_pi_io_width = 52.5;
_pi_io_z_off = 8;

_magnet_height = 3;
_magnet_radius = 5;

magnet_socket_floor = 1;
magnet_socket_wall = 2;

base_thickness_extra = 1;
post_thickness = 4;

expansion = 0.15;

$fn=90;

/* config end */

include <./deps/BOSL/constants.scad>
use <./deps/BOSL/transforms.scad>
use <./deps/BOSL/shapes.scad>

use <./lib/shapes.scad>;

pi_height = _pi_height + expansion;
pi_length = _pi_length + 2 * expansion;
pi_radius = _pi_radius + expansion;
pi_width = _pi_width + 2 * expansion;

pi_io_height = _pi_io_height + 2 * expansion;
pi_io_width = _pi_io_width + 2 * expansion;
pi_io_z_off = _pi_io_z_off - expansion;

magnet_height = _magnet_height + expansion;
magnet_radius = _magnet_radius + expansion;
magnet_socket_radius = magnet_radius + magnet_socket_wall;

base_thickness = magnet_socket_floor + magnet_height + base_thickness_extra;


corners = [
	[0, 0, 0, 0],
	[pi_width, 0, 0, 90],
	[pi_width, pi_length, 0, 180],
	[0, pi_length, 0, 270],
];


module quarter_tube(ir, h, wall) {
	or = ir + wall;

	translate([ir, ir]) {
		difference() {
			tube(ir=ir, wall=wall, h=h);

			left(or + 1)
			down(1)
			cube([2 * or + 2 , or + 1 , h + 2]);

			fwd(or + 1)
			down(1)
			cube([    or + 1 , or + 2 , h + 2]);
		}

		// rounded borders
		left(ir + wall / 2)
		cylinder(d=wall, h=h);

		fwd(ir + wall / 2)
		cylinder(d=wall, h=h);
	}
}

module magnet_casing() {
	cylinder(r=magnet_socket_radius, h=magnet_socket_floor);

	translate([0, 0, magnet_socket_floor])
	tube(ir=magnet_radius, wall=magnet_socket_wall, h=magnet_height + base_thickness_extra);

	// clips
	up((base_thickness_extra / 2) + (magnet_height + base_thickness_extra)) {
		right(magnet_radius)
		xrot(90)
		cylinder(d=base_thickness_extra, h=1, center=true);

		left(magnet_radius)
		xrot(90)
		cylinder(d=base_thickness_extra, h=1, center=true);
	}
}

union() {
	difference() {
		// base
		union() {
			// posts
			for (c = corners) {
				translate([c[0], c[1], c[2]])
				zrot(c[3])
				quarter_tube(ir=pi_radius, wall=post_thickness, h=pi_height + base_thickness);
			}

			// front
			hull() {
				quarter_tube(ir=pi_radius, wall=post_thickness / 2, h=base_thickness);

				right(pi_width)
				zrot(90)
				quarter_tube(ir=pi_radius, wall=post_thickness / 2, h=base_thickness);
			}

			// back
			back(pi_length)
			hull() {
				zrot(270)
				quarter_tube(ir=pi_radius, wall=post_thickness / 2, h=base_thickness);

				right(pi_width)
				zrot(180)
				quarter_tube(ir=pi_radius, wall=post_thickness / 2, h=base_thickness);
			}

			// bridge
			right((pi_width / 2) - (magnet_radius + magnet_socket_wall))
			cube([2 * (magnet_radius + magnet_socket_wall), pi_length, base_thickness]);
		}

		// I/O hole
		up(pi_io_z_off + base_thickness)
		right((pi_width - pi_io_width) / 2)
		fwd(5)
		cube([pi_io_width, 10, pi_io_height]);

		// magnets holes
		right(pi_width / 2)
		up(magnet_socket_floor)
		yspread(n=6, l=pi_length - 2 * magnet_socket_radius, sp=[0, magnet_socket_radius, 0]) {
			cylinder(r=magnet_radius, h=magnet_height + base_thickness_extra + 1);
		}
	}

	// magnet socket clips
	right(pi_width / 2)
	yspread(n=6, l=pi_length - 2 * magnet_socket_radius, sp=[0, magnet_socket_radius, 0]) {
		up((base_thickness_extra / 2) + (magnet_height + base_thickness_extra)) {
			right(magnet_radius)
			xrot(90)
			cylinder(d=base_thickness_extra, h=1, center=true);

			left(magnet_radius)
			xrot(90)
			cylinder(d=base_thickness_extra, h=1, center=true);
		}
	}
}
