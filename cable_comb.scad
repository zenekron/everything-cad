/* config */

pins = 8;
rows = 2;

wire_diameter = 4.00; // 4 to 4.25
wire_bump = 0.5;
inner_wall = 1;
outer_wall = 3;
height = 4;

$expansion = 0;
$fn=90;

/* config end */

include <./deps/BOSL/constants.scad>
use <./deps/BOSL/transforms.scad>
use <./deps/BOSL/shapes.scad>

cols = pins / rows;

id = wire_diameter + $expansion;
iw = inner_wall;
ow = outer_wall;


owl_x = ow;
owr_x = ow;
iws_x = (cols - 1) * iw;
walls_x = owl_x + owl_x + iws_x;
wires_x = cols * id;

owt_y = ow;
owb_y = 0.5 * iw;
iws_y = (rows - 1) * (0.5 * iw);
walls_y = owt_y + owb_y + iws_y;
wires_y = rows * id;

module base() {
	d2 = owt_y + id/2;
	d1 = walls_y + wires_y - d2;
	w1 = walls_x + wires_x;
	w2 = w1 - (owl_x + owr_x);

	up(height / 2)
	back(d1)
	right(w1 / 2)
	hull()
	{
		fwd(d1 / 2)
		cuboid([w1, d1, height], fillet=0.2, edges=EDGES_Z_ALL);

		back(d2 / 2)
		cube([w2, d2, height], center=true);
	}
}


difference()
{
	base();

	down(1)
	right(ow + 0.5 * id) {
		fwd(0.5 * id)
		for (c = [0 : cols - 1]) {
			right(c * (iw + id))
			left(0.5 * id - wire_bump)
			cube([id - 2 * wire_bump, owb_y + iws_y + wires_y, height + 2]);
		}

		back(0.5 * iw + 0.5 * id)
		for (r = [0 : rows - 1]) {
			back(r * (0.5 * iw + id))
			for (c = [0 : cols - 1]) {
				right(c * (iw + id))
				cylinder(d=id, h=height + 2);
			}
		}
	}
}
