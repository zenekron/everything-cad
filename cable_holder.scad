/* config */

height = 45;
width = 22.5;
depth = 10;
gap = 15;
wall = 7.5;

$fn=90;

/* config end */

include <./deps/BOSL/constants.scad>
use <./deps/BOSL/transforms.scad>
use <./deps/BOSL/shapes.scad>

id = width;
od = width + 2*wall;

module hook() {
	right(od / 2)
	difference()
	{
		tube(id=id, wall=wall, h=depth);

		left(od / 2 + 1)
		down(1)
		cube([od + 2, width, depth + 2]);
	}
}

module walls() {
	short_height = height - gap - wall/2;

	// left
	cube([wall, short_height, depth]);
	back(short_height)
	right(wall / 2)
	cylinder(d=wall, h=depth);

	// right
	right(id + wall)
	cube([wall, height, depth]);
}

module base() {
	base_width = od * 1.5;

	back(height + wall / 2)
	hull()
	{
		right(wall / 2)
		cylinder(d=wall, h=depth);
		cube([wall / 2, wall / 2, depth]);


		right(base_width)
		{
			cylinder(d=wall, h=depth);
			cube([wall / 2, wall / 2, depth]);
		}
	}

	// fillets
	up(depth / 2)
	back(height)
	{
		right(od - wall)
		zrot(90)
		interior_fillet(l=depth, r=wall, orient=ORIENT_ZNEG);

		right(od)
		zrot(180)
		interior_fillet(l=depth, r=wall, orient=ORIENT_ZNEG);
	}
}

hook();
walls();
base();
