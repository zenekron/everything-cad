module rounded_cube(size, fillet=0, center=false) {
	// sizes
	width = size[0];
	depth = size[1];
	height = size[2];

	// corners
	x_step = width - 2 * fillet;
	y_step = depth - 2 * fillet;
	corners = [[0, 0], [x_step, 0], [x_step, y_step], [0, y_step]];

	// offset
	off = center ? [fillet - width / 2, fillet - depth / 2] : [fillet, fillet];

	// render
	translate(off)
	hull() {
		for (p = corners) {
			translate(p)
			cylinder(r=fillet, h=height);
		}
	}
}
