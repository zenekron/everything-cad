module hex_bit(ir=undef, or=undef, h) {
	assert(!is_undef(ir) || !is_undef(or), "ir or or must be specified");

	or = is_undef(or) ? ir / cos(30) : or;
	cylinder(r=or, h=h, $fn=6);
}
